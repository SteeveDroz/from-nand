#include "booleans.h"

// The only thing that is actually given from C++
bool NAND(const bool a, const bool b) {
    return !(a & b);
}

bool NOT(const bool a) {
    return NAND(a, a);
}

bool AND(const bool a, const bool b) {
    return NAND(NAND(a, b), NAND(a, b));
}

bool OR(const bool a, const bool b) {
    return NAND(NOT(a), NOT(b));
}

bool XOR(const bool a, const bool b) {
    return AND(NAND(a, b), OR(a, b));
}

bool EQUALS(const bool a, const bool b) {
    return NOT(XOR(a, b));
}
