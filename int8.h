#ifndef _INT8_H_
#define _INT8_H_

#include "booleans.h"
#include "bytes.h"

#include <iostream>

struct int8 {
    bool data[8];
    bool isNaN;

    static int8 ZERO;
    static int8 ONE;
    static int8 NaN;

    int8();
    int8(bool b0, bool b1, bool b2, bool b3, bool b4, bool b5, bool b6, bool b7);
    int8(bool isNaN);

    int8 operator-();
    int8 operator+(int8 other);
    int8 operator-(int8 other);
    int8 operator*(int8 other);
    int8 operator/(int8 other);
    int8 operator<<(int8 quantity);
    int8 operator>>(int8 quantity);
    bool operator==(int8 other);
    bool operator!=(int8 other);
    bool operator<(int8 other);
    bool operator>(int8 other);
    bool operator<=(int8 other);
    bool operator>=(int8 other);
    int8& operator=(int8 other);
    char getDigit(int8 i8);
    void toCharArray(char *result);
};

std::ostream& operator<<(std::ostream& stream, int8 i8);

#endif
