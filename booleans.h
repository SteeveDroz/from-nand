#ifndef _BOOLEAN_H_
#define _BOOLEAN_H_

bool NAND(const bool a, const bool b);
bool NOT(const bool a);
bool AND(const bool a, const bool b);
bool OR(const bool a, const bool b);
bool XOR(const bool a, const bool b);
bool EQUALS(const bool a, const bool b);

#endif
