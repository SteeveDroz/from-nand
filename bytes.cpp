#include "bytes.h"

void halfAdder(const bool a, const bool b, bool *result, bool *co) {
    *result = XOR(a, b);
    *co = AND(a, b);
}

void fullAdder(const bool a, const bool b, const bool ci, bool *result, bool *co) {
    bool r1;
    bool c1;
    bool r2;
    bool c2;

    halfAdder(a, b, &r1, &c1);
    halfAdder(r1, ci, &r2, &c2);

    *result = r2;
    *co = OR(c1, c2);
}

bool byteAdder(const bool *a, const bool *b, bool *result) {
    bool carry = false;
    fullAdder(a[7], b[7], carry, &result[7], &carry);
    fullAdder(a[6], b[6], carry, &result[6], &carry);
    fullAdder(a[5], b[5], carry, &result[5], &carry);
    fullAdder(a[4], b[4], carry, &result[4], &carry);
    fullAdder(a[3], b[3], carry, &result[3], &carry);
    fullAdder(a[2], b[2], carry, &result[2], &carry);
    fullAdder(a[1], b[1], carry, &result[1], &carry);
    fullAdder(a[0], b[0], carry, &result[0], &carry);
    return carry;
}

bool LSHIFT(const bool *a, bool *result) {
    bool carry = a[0];
    result[0] = a[1];
    result[1] = a[2];
    result[2] = a[3];
    result[3] = a[4];
    result[4] = a[5];
    result[5] = a[6];
    result[6] = a[7];
    result[7] = 0;
    return carry;
}

bool RSHIFT(const bool *a, bool *result) {
    result[7] = a[6];
    result[6] = a[5];
    result[5] = a[4];
    result[4] = a[3];
    result[3] = a[2];
    result[2] = a[1];
    result[1] = a[0];
    result[0] = 0;
    return false;
}

void complementTo1(const bool *a, bool *result) {
    result[0] = NOT(a[0]);
    result[1] = NOT(a[1]);
    result[2] = NOT(a[2]);
    result[3] = NOT(a[3]);
    result[4] = NOT(a[4]);
    result[5] = NOT(a[5]);
    result[6] = NOT(a[6]);
    result[7] = NOT(a[7]);
}

void complementTo2(const bool *a, bool *result) {
    bool one[8] = { false, false, false, false, false, false, false, true };
    bool tmp[8];

    complementTo1(a, tmp);
    byteAdder(tmp, one, result);
}

bool EQUAL(const bool *a, const bool *b) {
    if (NOT(EQUALS(a[0], b[0])))
        return false;
    if (NOT(EQUALS(a[1], b[1])))
        return false;
    if (NOT(EQUALS(a[2], b[2])))
        return false;
    if (NOT(EQUALS(a[3], b[3])))
        return false;
    if (NOT(EQUALS(a[4], b[4])))
        return false;
    if (NOT(EQUALS(a[5], b[5])))
        return false;
    if (NOT(EQUALS(a[6], b[6])))
        return false;
    if (NOT(EQUALS(a[7], b[7])))
        return false;
    return true;
}
