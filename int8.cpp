#include "int8.h"

int8 int8::ZERO(false, false, false, false, false, false, false, false);
int8 int8::ONE(false, false, false, false, false, false, false, true);
int8 int8::NaN(true);

int8::int8() : int8(false, false, false, false, false, false, false, false) {}

int8::int8(bool b0, bool b1, bool b2, bool b3, bool b4, bool b5, bool b6, bool b7)
{
    data[0] = b0;
    data[1] = b1;
    data[2] = b2;
    data[3] = b3;
    data[4] = b4;
    data[5] = b5;
    data[6] = b6;
    data[7] = b7;
    isNaN = false;
}

int8::int8(bool isNaN) : int8()
{
    this->isNaN = isNaN;
}

int8 int8::operator-()
{
    if (isNaN)
    {
        return int8::NaN;
    }

    int8 opposite;
    complementTo2(this->data, opposite.data);
    return opposite;
}

int8 int8::operator+(int8 other)
{
    if (OR(this->isNaN, other.isNaN))
    {
        return int8::NaN;
    }

    int8 result;
    byteAdder(this->data, other.data, result.data);
    return result;
}

int8 int8::operator-(int8 other)
{
    return *this + (-other);
}

int8 int8::operator*(int8 other)
{
    if (OR(this->isNaN, other.isNaN))
    {
        return int8::NaN;
    }

    bool negative = false;
    int8 positiveThis;
    int8 positiveOther;
    int8 result = int8::ZERO;
    if (-*this > *this)
    {
        negative = NOT(negative);
        positiveThis = -*this;
    }
    else
    {
        positiveThis = *this;
    }

    if (-other > other)
    {
        negative = NOT(negative);
        positiveOther = -other;
    }
    else
    {
        positiveOther = other;
    }

    while (positiveOther > int8::ZERO)
    {
        result = result + positiveThis;
        positiveOther = positiveOther - int8::ONE;
    }

    if (negative)
    {
        result = -result;
    }

    if (XOR(negative, result < int8::ZERO))
    {
        return int8::NaN;
    }

    return result;
}

int8 int8::operator/(int8 other)
{
    if (OR(this->isNaN, other.isNaN))
    {
        return int8::NaN;
    }

    if (other == int8::ZERO)
    {
        return int8::NaN;
    }

    if (*this == other)
    {
        return int8::ONE;
    }

    if (other == int8::ONE)
    {
        return *this;
    }

    int8 dividend;
    if (*this < int8::ZERO)
    {
        dividend = -*this;
    }
    else
    {
        dividend = *this;
    }

    int8 divisor;
    if (other < int8::ZERO)
    {
        divisor = -other;
    }
    else
    {
        divisor = other;
    }

    int8 multiplier = int8::ONE;
    int8 quotient = int8::ZERO;

    while ((multiplier * divisor) < dividend)
    {
        multiplier = multiplier << int8::ONE;
    }
    multiplier = multiplier >> int8::ONE;

    while (multiplier > int8::ONE)
    {
        if (dividend >= (divisor * multiplier))
        {
            quotient = quotient + int8::ONE;
            dividend = dividend - divisor * multiplier;
        }
        multiplier = multiplier >> int8::ONE;
        quotient = quotient << int8::ONE;
    }

    if (XOR(*this < int8::ZERO, other < int8::ZERO))
    {
        quotient = -quotient;
    }
    return quotient;
}

bool int8::operator==(int8 other)
{
    if (OR(this->isNaN, other.isNaN))
    {
        return false;
    }

    return EQUAL(this->data, other.data);
}

bool int8::operator!=(int8 other)
{
    if (OR(this->isNaN, other.isNaN))
    {
        return true;
    }

    return NOT(*this == other);
}

bool int8::operator<(int8 other)
{
    if (OR(this->isNaN, other.isNaN))
    {
        return false;
    }

    if (AND(EQUALS(this->data[0], false), EQUALS(other.data[0], true))) {
        return false;
    }

    if (AND(EQUALS(this->data[0], true), EQUALS(other.data[0], false))) {
        return true;
    }

    if (AND(EQUALS(this->data[0], true), EQUALS(other.data[0], true))) {
        return -other < -*this;
    }

    if (AND(EQUALS(this->data[1], true), EQUALS(other.data[1], false))) {
        return false;
    }
    if (AND(EQUALS(this->data[1], false), EQUALS(other.data[1], true))) {
        return true;
    }
    if (AND(EQUALS(this->data[2], true), EQUALS(other.data[2], false))) {
        return false;
    }
    if (AND(EQUALS(this->data[2], false), EQUALS(other.data[2], true))) {
        return true;
    }
    if (AND(EQUALS(this->data[3], true), EQUALS(other.data[3], false))) {
        return false;
    }
    if (AND(EQUALS(this->data[3], false), EQUALS(other.data[3], true))) {
        return true;
    }
    if (AND(EQUALS(this->data[4], true), EQUALS(other.data[4], false))) {
        return false;
    }
    if (AND(EQUALS(this->data[4], false), EQUALS(other.data[4], true))) {
        return true;
    }
    if (AND(EQUALS(this->data[5], true), EQUALS(other.data[5], false))) {
        return false;
    }
    if (AND(EQUALS(this->data[5], false), EQUALS(other.data[5], true))) {
        return true;
    }
    if (AND(EQUALS(this->data[6], true), EQUALS(other.data[6], false))) {
        return false;
    }
    if (AND(EQUALS(this->data[6], false), EQUALS(other.data[6], true))) {
        return true;
    }
    if (AND(EQUALS(this->data[7], true), EQUALS(other.data[7], false))) {
        return false;
    }
    if (AND(EQUALS(this->data[7], false), EQUALS(other.data[7], true))) {
        return true;
    }

    return false;
}

bool int8::operator>(int8 other)
{
    return other < *this;
}

bool int8::operator<=(int8 other)
{
    return OR(*this == other, *this < other);
}

bool int8::operator>=(int8 other) {
    return OR(*this == other, *this > other);
}

int8 int8::operator<<(int8 quantity)
{
    if (OR(this->isNaN, quantity.isNaN))
    {
        return int8::NaN;
    }

    int8 result = *this;
    while (quantity > int8::ZERO)
    {
        int8 shift;
        LSHIFT(result.data, shift.data);
        result = shift;
        quantity = quantity - int8::ONE;
    }
    return result;
}

int8 int8::operator>>(int8 quantity)
{
    if (OR(this->isNaN, quantity.isNaN))
    {
        return int8::NaN;
    }

    int8 result = *this;
    while (quantity > int8::ZERO)
    {
        int8 shift;
        RSHIFT(result.data, shift.data);
        result = shift;
        quantity = quantity - int8::ONE;
    }
    return result;
}

int8& int8::operator=(int8 other)
{
    if (other.isNaN)
    {
        this->isNaN = true;
        return *this;
    }

    this->data[0] = other.data[0];
    this->data[1] = other.data[1];
    this->data[2] = other.data[2];
    this->data[3] = other.data[3];
    this->data[4] = other.data[4];
    this->data[5] = other.data[5];
    this->data[6] = other.data[6];
    this->data[7] = other.data[7];
    return *this;
}

void int8::toCharArray(char *result)
{
    int8 number = *this;
    int8 digits[3];

    int8 ten(false, false, false, false, true, false, true, false);
    int8 power = int8(false, true, false, false, false, true, false, false);
    while (number < power)
    {
        power = power / ten;
    }

    int8 digit;

    digit = number / power;
    digits[0] = digit;
    number = number - (digit * power);
    power = power / ten;

    digit = number / power;
    digits[1] = digit;
    number = number - digit * power;
    power = power / ten;

    digit = number / power;
    digits[2] = digit;
    number = number - digit * power;
    power = power / ten;

    result[0] = getDigit(digits[0]);
    result[1] = getDigit(digits[1]);
    result[2] = getDigit(digits[2]);
}

char int8::getDigit(int8 i8)
{
    if (i8 == int8(false, false, false, false, true, false, false, true))
    {
        return '9';
    }
    else if (i8 == int8(false, false, false, false, true, false, false, false))
    {
        return '8';
    }
    else if (i8 == int8(false, false, false, false, false, true, true, true))
    {
        return '7';
    }
    else if (i8 == int8(false, false, false, false, false, true, true, false))
    {
        return '6';
    }
    else if (i8 == int8(false, false, false, false, false, true, false, true))
    {
        return '5';
    }
    else if (i8 == int8(false, false, false, false, false, true, false, false))
    {
        return '4';
    }
    else if (i8 == int8(false, false, false, false, false, false, true, true))
    {
        return '3';
    }
    else if (i8 == int8(false, false, false, false, false, false, true, false))
    {
        return '2';
    }
    else if (i8 == int8(false, false, false, false, false, false, false, true))
    {
        return '1';
    }
    else if (i8 == int8(false, false, false, false, false, false, false, false))
    {
        return '0';
    }

    return '\0';
}

std::ostream & operator<<(std::ostream &stream, int8 i8)
{
    if (i8.isNaN)
    {
        stream << "NaN";
    }
    else
    {
        char digits[3];
        i8.toCharArray(digits);
        stream << digits[0] << digits[1] << digits[2];
        //stream << i8.data[0] << i8.data[1] << i8.data[2] << i8.data[3] << i8.data[4] << i8.data[5] << i8.data[6] << i8.data[7];
    }
    return stream;
}
