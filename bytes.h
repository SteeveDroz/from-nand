#ifndef _BYTES_H_
#define _BYTES_H_

#include "booleans.h"

void halfAdder(const bool a, const bool b, bool* result, bool* co);
void fullAdder(const bool a, const bool b, const bool ci, bool* result, bool* co);
bool byteAdder(const bool *a, const bool *b, bool *result);
bool LSHIFT(const bool *a, bool *result);
bool RSHIFT(const bool *a, bool *result);
void complementTo1(const bool *a, bool *result);
void complementTo2(const bool *a, bool *result);
bool EQUAL(const bool *a, const bool *b);

#endif
