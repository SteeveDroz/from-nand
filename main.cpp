#include "main.h"

using namespace std;

int main(int argc, char** argv) {
    int8 zero(false, false, false, false, false, false, false, false);
    int8 one(false, false, false, false, false, false, false, true);
    int8 two(false, false, false, false, false, false, true, false);
    int8 three(false, false, false, false, false, false, true, true);
    int8 four(false, false, false, false, false, true, false, false);
    int8 _253(true, true, true, true, true, true, false, true);
    int8 _42(false, false, true, false, true, false, true, false);

    int8 n = _42;
    cout << "FORTY-TWO" << endl;
    // cout << n << endl;
    cout << _253 << endl;
    return 0;
}
